# MS2 Support Files

## O1

- Report for [August 19th](https://gitlab.com/sguzmanm/isis3510_201920_p_s.guzmanm/wikis/ISIS2510-MS2-O1-Point-2): Cause effect diagram and solutions.

## O2

- Personas delivery: All files are exported from draw.io. If they need to be seen you can import them in a new draw.io file.
  - [Passive vendor](./Persona01_PassiveVendor.drawio)
  - [Active vendor](./Persona02_Active_Vendor.drawio)
  - [Sports woman](./Persona03_SportsWoman.drawio)
  - [Male visitor](./Persona04_VisitorMale.drawio)

## O3

- Reports reviewed: 
  - Julián Manrique: https://gitlab.com/Sxubas/isis3510_201920_p_ja.manrique/issues/4  
  - Jorge Gómez: https://gitlab.com/jandresgomez16/isis3510_201920_p_ja.gomez1/issues/2
  - maria del Rosario León: https://gitlab.com/mdr.leon10/isis3510_201920_p_mdr.leon10/wikis/Define-and-Ideate
- Personas delivery: All files are exported from draw.io. If they need to be seen you can import them in a new draw.io file.
  - [Passive vendor](./Persona01_PassiveVendor.drawio)
  - [Active vendor](./Persona02_Active_Vendor.drawio)
  - [Sports woman](./Persona03_SportsWoman.drawio)
  - [Male visitor](./Persona04_VisitorMale.drawio)
- Context canvas: The files is exported from draw.io. If it needs to be seen you can import it in a new draw.io file. The context canvas is defined taking into account the personas in the previous delivery. [Deliverable](./Context_Canvas.drawio)
- Report: Final report for [August 24th](https://gitlab.com/sguzmanm/isis3510_201920_p_s.guzmanm/wikis/MS2)
